import PackageDescription

let package = Package(
    name: "swift-http-server",
    targets: [
    	Target(name: "swift-http"),
    	Target(name: "swift-http-server", dependencies: ["swift-http"])
    ],
    dependencies: [
        .Package(url: "https://bitbucket.org/atlassian/swift-gcd-server.git", majorVersion: 0)
    ]
)
