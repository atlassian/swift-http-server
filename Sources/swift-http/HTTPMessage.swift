//
//  HTTPMessage.swift
//  swift-http-server
//
//  Created by Nikolay Petrov on 12/8/16.
//
//

import Foundation

public protocol HTTPMessage {
    var headers: [String: String] { get }
}

// General headers
public extension HTTPMessage {
    var CacheControl: String? {
        return headers["Cache-Control"]
    }

    var Connection: String? {
        return headers["Connection"]
    }

    var Date: String? {
        return headers["Date"]
    }

    var Pragma: String? {
        return headers["Pragma"]
    }

    var TransferEncoding: String? {
        return headers["Transfer-Encoding"]
    }

    var Upgrade: String? {
        return headers["Upgrade"]
    }

    var Via: String? {
        return headers["Via"]
    }
}

// Entity headers
public extension HTTPMessage {
    var Allow: String? {
        return headers["Allow"]
    }

    var ContentBase: String? {
        return headers["Content-Base"]
    }

    var ContentEncoding: String? {
        return headers["Content-Encoding"]
    }

    var ContentLanguage: String? {
        return headers["Content-Language"]
    }

    var ContentLength: Int? {
        return headers["Content-Length"].flatMap { Int($0) }
    }

    var ContentLocation: URL? {
        return headers["Content-Location"].flatMap { URL(string: $0) }
    }

    var ContentMD5: String? {
        return headers["Content-MD5"]
    }

    var ContentRange: String? {
        return headers["Content-Range"]
    }

    var ContentType: String? {
        return headers["Content-Type"]
    }

    var ETag: String? {
        return headers["ETag"]
    }

    var Expires: String? {
        return headers["Expires"]
    }

    var LastModified: String? {
        return headers["Last-Modified"]
    }
}
